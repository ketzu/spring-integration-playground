package dev.tesolva.playground;

import dev.tesolva.playground.file.FileEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.dsl.ConsumerEndpointSpec;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.file.FileReadingMessageSource;
import org.springframework.integration.jpa.dsl.Jpa;
import org.springframework.integration.jpa.support.PersistMode;

import javax.persistence.EntityManagerFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.Random;

@SpringBootApplication
@EnableIntegration
public class PlaygroundApplication {

    public static void main(String[] args) {
        // Normal spring boot startup
        SpringApplication.run(PlaygroundApplication.class, args);
    }

    @Autowired
    private EntityManagerFactory eMF;

    // Directory to "watch" (only recognize new files)
    public String INPUT_DIR = "C:\\Examples";

    /**
     * Creates the message source for the integration, will recognize new files and input them in the channel.
     */
    @Bean
    public MessageSource<File> fileCreateSource() {
        var sourceReader = new FileReadingMessageSource();
        sourceReader.setDirectory(new File(INPUT_DIR));
        sourceReader.setUseWatchService(true);
        return sourceReader;
    }

    /**
     * Defines the flow from the directory source to the Jpa entity repository
     */
    @Bean
    public IntegrationFlow newFileToJPAFlow() {
        return IntegrationFlows.from(
                        fileCreateSource(),
                        configurer -> configurer.poller(Pollers.fixedDelay(10000))
                )
                .log()
                .transform(file -> new FileEntity(((File) file).getName()))
                .handle(Jpa.outboundAdapter(this.eMF)
                                .entityClass(FileEntity.class)
                                .persistMode(PersistMode.PERSIST),
                        ConsumerEndpointSpec::transactional)
                .get();
    }

    /**
     * Define a flow that randomly changes things in the JPA entities for fun and auditing
     */
    @Bean
    public IntegrationFlow jpaChangeFlow() {
        return IntegrationFlows.from(Jpa.inboundAdapter(this.eMF)
                                .entityClass(FileEntity.class),
                        e -> e.poller(Pollers.fixedDelay(10000)))
                .transform(message -> { // select random element from list
                    @SuppressWarnings("unchecked")
                    var list = ((ArrayList<FileEntity>) message);
                    var index = new Random().nextInt(list.size());
                    System.out.println(index);
                    return list.get(index);
                })
                .transform(file -> {
                    ((FileEntity) file).setModification(new Random().nextInt());
                    return file;
                })
                .handle(Jpa.outboundAdapter(this.eMF)
                                .entityClass(FileEntity.class)
                                .persistMode(PersistMode.MERGE),
                        ConsumerEndpointSpec::transactional)
                .get();
    }
}
