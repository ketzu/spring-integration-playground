package dev.tesolva.playground.file;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.history.Revision;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * File service that is use for the file controller to fetch files from the repository.
 */
@Service
public class FileService {

    private final FileRepository fileRepository;

    @Autowired
    public FileService(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    public List<FileEntity> getFiles() {
        return fileRepository.findAll();
    }

    public void addFile(FileEntity entity) {
        fileRepository.save(entity);
    }

    public List<Revision<Integer, FileEntity>> getRevisions(Long id) {
        return fileRepository.findRevisions(id).getContent();
    }
}
