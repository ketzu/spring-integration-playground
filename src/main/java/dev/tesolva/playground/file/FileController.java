package dev.tesolva.playground.file;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.history.Revision;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Traditional Spring rest contorller that produces a json list of files in DB.
 * Could theoretically be replaces with another integrationflow that takes entities from the db to a http gateway.
 */
@RestController
@RequestMapping(path = "api/v1/file")
public class FileController {

    private final FileService fileService;

    @Autowired
    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping
    public List<FileEntity> getFiles() {
        return fileService.getFiles();
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    @ResponseBody
    public List<Revision<Integer, FileEntity>> getRevision(@PathVariable("id") Long id) {
        return this.fileService.getRevisions(id);
    }
}
