package dev.tesolva.playground.file;

import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.time.LocalDateTime;

/*
 * Entity to model files in DB.
 */
@Entity
@Table
@Audited
public class FileEntity {
    @Id
    @SequenceGenerator(
            name = "file_sequence",
            sequenceName = "file_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "file_sequence"
    )
    private Long id;

    private String name;
    private Integer modification;
    private LocalDateTime registered;

    public FileEntity() {
    }

    public FileEntity(String name) {
        this.name = name;
        this.registered = LocalDateTime.now();
        this.modification = 0;
    }

    public FileEntity(String name, LocalDateTime registered) {
        this.name = name;
        this.registered = registered;
        this.modification = 0;
    }

    public FileEntity(Long id, String name, LocalDateTime registered, Integer modification) {
        this.id = id;
        this.name = name;
        this.registered = registered;
        this.modification = modification;
    }

    @Override
    public String toString() {
        return "File{" +
                "id=" + id +
                ", name='" + name +
                ", registered=" + registered +
                ", modification=" + modification +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getRegistered() {
        return registered;
    }

    public void setRegistered(LocalDateTime registered) {
        this.registered = registered;
    }

    public Integer getModification() {
        return modification;
    }

    public void setModification(Integer modification) {
        this.modification = modification;
    }
}
