package dev.tesolva.playground.file;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * File configuration just used for demo purposes and initial trials.
 */
@Configuration
public class FileConfiguration {

    @Bean
    CommandLineRunner clRunner(FileRepository fileRepository) {
        return args -> {

            //var file = new FileEntity("useless.csv");
            //var file2 = new FileEntity("another.csv");

            //fileRepository.saveAll(List.of(file, file2));

        };
    }
}
