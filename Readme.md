# Spring Integration Playground

I started this project to get used to spring integration and spring in general.

## What does it do?

It "watches" a directory (hardcoded to 'c:\Examples') and puts the notification date and filename into a databse. File
deletions are not registered or processed. Files may or may not be registered multiple times.

All previous registered files can be checked out via a http get request to 'api/v1/file'. The response should be JSON.

Using spring-envers the program keeps track of all changes to entities. As no modifications are detected on the files,
it performs random modifications to create a history.

The history can be accessed via 'api/v1/file/<fileid>', e.g., 'api/va/file/1'.

## What is where?

[`src/main/java/dev/tesolva/playground`](src/main/java/dev/tesolva/playground) is the main location of all files.

[PlaygroundApplication.java](src/main/java/dev/tesolva/playground/PlaygroundApplication.java) contains the main method
for starting the application as well as the Integration parts.

Everything related to the controller and Jpa is stored in the [file](src/main/java/dev/tesolva/playground/file) package.

* [FileEntity.java](src/main/java/dev/tesolva/playground/file/FileEntity.java) contains the Entity (@Entity) definition.
* [FileConfiguration.java](src/main/java/dev/tesolva/playground/file/FileConfiguration.java) only used for initial
  trials to generate some dummy data.
* [FileRepository.java](src/main/java/dev/tesolva/playground/file/FileRepository.java) is the repository (using
  @Repository)
  interface for spring (effectively the data access layer?).
* [FileService.java](src/main/java/dev/tesolva/playground/file/FileService.java) contains the service level
  abstraction (
  @Service) including looking up all files for the controller.
* [FileController.java](src/main/java/dev/tesolva/playground/file/FileController.java) defines the traditional rest
  controller including the getmapping and path.

As of now there are no tests.

## Possibly Surprising Dependencies

This repository uses H2 as a database with a configured database file of h2playground.

Spring, Spring Integration etc. should not be surprising.

## How to build?

`gradlew[.bat] build`